package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;


public class MyLeadsPage extends Annotations {
	
	public MyLeadsPage() {
	       PageFactory.initElements(driver, this);
		} 
		@CacheLookup
		@FindBy(how=How.LINK_TEXT, using="Create Lead") 
		WebElement eleCreateLead;

		public CreateLeadPage clickCreateLead() {
			click(eleCreateLead);
			return new CreateLeadPage();
		}
/*		
		public FindLeadPage clickFindLead() {
			driver.findElementByXPath("//a[text()='Find Leads']").click();
			return new FindLeadPage();
		}
		
		public MergeLeadPage clickMergeLead() {
			driver.findElementByXPath("//a[text()='Find Leads']").click();
			return new MergeLeadPage();
			
		}
*/
}
