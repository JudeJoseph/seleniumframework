package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage() {
	       PageFactory.initElements(driver, this);
		} 
		@CacheLookup
		@FindBy(how=How.ID, using="createLeadForm_companyName") 
		WebElement eleCompanyName;
		@FindBy(how=How.ID, using="createLeadForm_firstName") 
		WebElement eleFirstName;
		@FindBy(how=How.ID, using="createLeadForm_lastName") 
		WebElement eleLastName;
		@FindBy(how=How.CLASS_NAME, using="smallSubmit") 
		WebElement eleCreateLeadButton;
	@Given("enter the Company Name as (.*)")
	public CreateLeadPage typeCompanyName(String cName) {
		clearAndType(eleCompanyName, cName);  
		return this;
	}
	@Given("enter the First Name as (.*)")	
	public CreateLeadPage typeFirstName(String fName) {
		clearAndType(eleFirstName, fName);  
		return this;
	}
	@Given("enter the Last Name as (.*)")
	public CreateLeadPage typeLastName(String lName) {
		clearAndType(eleLastName, lName);  
		return this;
	}
	@When("click on the Create Lead Button")
	public ViewLeadPage clickCreateLeadButton() {
		click(eleCreateLeadButton);
		return new ViewLeadPage();
	}

}
