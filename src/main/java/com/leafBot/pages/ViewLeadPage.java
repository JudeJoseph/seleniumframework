package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ViewLeadPage extends Annotations{
	
	public ViewLeadPage() {
	       PageFactory.initElements(driver, this);
		} 
		@CacheLookup
		@FindBy(how=How.ID, using="viewLead_firstName_sp") 
		WebElement eleFirstName;
		@FindBy(how=How.ID, using="viewLead_companyName_sp") 
		WebElement eleLeadId;
		
	public static String leadId;
	@Then("Verify FistName as (.*)")
	public ViewLeadPage verifyFirstName(String fName) {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals(fName)) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	
	public MyLeadsPage clickDeleteButton() {
		leadId = driver.findElementById("").getText().replaceAll("[^0-9]", "");
		driver.findElementByXPath("//a[text()='Delete']").click();
		return new MyLeadsPage();
	}

	
	

}
