package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.CacheLookup;
import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class HomePage extends Annotations{ 

	public HomePage() {
       PageFactory.initElements(driver, this);
	} 
	@CacheLookup
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") 
	WebElement eleLogout;
	@FindBy(how=How.LINK_TEXT, using="Leads") 
	WebElement eleLeadsTab;
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") 
	WebElement eleCRMSFALink;
	
	
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
		return new LoginPage();
	}
	@When("click on Create Lead Link")
	public MyLeadsPage clickLeadsTab() {
		click(eleLeadsTab);
		return new MyLeadsPage();
	}
	@When("click on the CRMSFALink")
	public HomePage clickCRMSFALink() {
		click(eleCRMSFALink);
		return this;
	}
	
	
}







