Feature: CreateLead

Background:
Given Lauch The Browser
And Enter the Username as DemoSalesManager
And Enter the password as crmsfa
When Click on the login button
And click on the CRMSFALink
And click on Create Lead Link

Scenario Outline: TC001 Create Lead
Given enter the Company Name as <cName>
And enter the First Name as <fName>
And enter the Last Name as <lName>
When click on the Create Lead Button
Then Verify FistName

Examples:
|cName|fName|lName|
|Standard|Jude|Joseph|
|Infosys|Dinesh|Kumar|
